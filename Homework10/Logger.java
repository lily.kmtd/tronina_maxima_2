package Homework10;

//Задание 10 - Logger
//Сделать класс Logger с методом void log(String message), который выводит в консоль какое-либо сообщение.
//Применить паттерн Singleton для Logger.

public class Logger {
    private final static Logger instance;//единственны экземпляр тк final
    static {
        instance = new Logger();
    }

    public static Logger getInstance() {//вместо конструктора - доступ к единственному экземпляру
        return instance;
    }

    public void log(String message) {
        System.out.println("log: "+ message);
    }

}
