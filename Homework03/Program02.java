package Homework03;

import java.util.Scanner;

public class Program02 {
    //Задание 03 - Sequence
    //Program2 - необходимо найти количество локальных минимумов
    public static void main(String []args){
        int endOfLine = -1;
        int localMinsCount = 0;

        int previousNumber = 0;
        int number = 0;
        int nextNumber = 0;

        Scanner scanner = new Scanner(System.in);
        previousNumber = scanner.nextInt();
        number = scanner.nextInt();
        nextNumber = scanner.nextInt();

        while (nextNumber != endOfLine) {
            if (number < previousNumber && number < nextNumber) {
                localMinsCount ++;
            }

            previousNumber = number;
            number = nextNumber;
            nextNumber = scanner.nextInt();
        }
        System.out.println("Число локальных минимумов: " + localMinsCount);
    }
}
