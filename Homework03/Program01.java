package Homework03;

import java.util.Scanner;

public class Program01 {

//    Задание 03 - Sequence
//На вход подается последовательность чисел:aN = -1
//Program1 - необходимо найти число, у которого сумма цифр минимальная среди всех остальных.

    public static void main(String[] args) {
        int endOfLine = -1;
        int minDigitSumNumber = 0;
        int minDigitSumValue = 2147483647;//int max value;

        int enteredNumber = 0;
        Scanner scanner = new Scanner(System.in);
        while (enteredNumber != endOfLine) {
            enteredNumber = scanner.nextInt();

            //calc digit sum
            int toCalcDigitSumNumber = enteredNumber;
            int digitsSum = 0;
            while (toCalcDigitSumNumber != 0) {
                int lastDigit = toCalcDigitSumNumber % 10;
                toCalcDigitSumNumber = toCalcDigitSumNumber / 10;
                digitsSum = digitsSum + lastDigit;
            }

            //find min digit sum
            if (digitsSum < minDigitSumValue) {
                minDigitSumNumber = enteredNumber;
                minDigitSumValue = digitsSum;
            }
        }
        System.out.println("Число с минимальной суммой цифр: " + minDigitSumNumber);
    }
}
