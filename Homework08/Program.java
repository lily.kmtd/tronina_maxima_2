package Homework08;

import java.util.Scanner;

public class Program {
    //Задание 08 - Recursion
    //Написать рекурсивную функцию int sumOfDigits(int number) - возвращает сумму цифр числа.
    public static void main(String []args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int digitsSum = 0;
        digitsSum = sumOfDigits(n);
        System.out.println("Cумма цифр числа: " + digitsSum);
    }

    static int sumOfDigits(int number) {
        if (number < 10) {
            return number;
        }
        return  number % 10 + sumOfDigits(number / 10);
    }
}
