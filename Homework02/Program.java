package Homework02;

import java.util.Scanner;
//Задание 02 - Variables
//    Гарантируется, что 0 <= number <= 127
//    Нельзя использовать массивы, циклы, строки, Integer.


public class Program {
    public static void main(String[] args) {
        int base = 2;
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();

        int bit1, bit2, bit3, bit4, bit5, bit6, bit7, bit8;
        bit1 =  bit2 = bit3 = bit4 = bit5 = bit6 = bit7 = bit8 = 0;

        if (number != 0) {
            bit8 = number % base;
            number = number / base;
        }
        if (number != 0) {
            bit7 = number % base;
            number = number / base;
        }
        if (number != 0) {
            bit6 = number % base;
            number = number / base;
        }
        if (number != 0) {
            bit5 = number % base;
            number = number / base;
        }
        if (number != 0) {
            bit4 = number % base;
            number = number / base;
        }
        if (number != 0) {
            bit3 = number % base;
            number = number / base;
        }
        if (number != 0) {
            bit2 = number % base;
            number = number / base;
        }
        if (number != 0) {
            bit1 = number % base;
            number = number / base;
        }
        System.out.println(bit1+""+bit2+""+bit3+""+bit4+""+bit5+""+bit6+""+bit7+""+bit8);
    }
}
