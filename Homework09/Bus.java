package Homework09;

/**
 * 30.10.2021
 * 12. Bus
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Bus {
//    Когда автобус едет, все его пассажири называют свои имена.
//    Когда автобус едет, нельзя менять водителя, нельзя, чтобы пассажиры покидали свои места,
//    нельзя, чтобы автобус принимал пассажиров.

    private boolean isGoing;
    private int number;
    private String model;
    private Driver driver;

    // массив пассажиров
    private Passenger[] passengers;
    // фактическое количество пассажиров на данный момент
    private int count;

    public Bus(int number, String model, int placesCount) {
        this.isGoing = false;

        if (number > 0) {
            this.number = number;
        } else {
            this.number = 1;
        }
        this.model = model;
        // создали placesCount объектных переменных в которые можно положить пассажира
        this.passengers = new Passenger[placesCount];
    }

    public void incomePassenger(Passenger passenger) {
        if (this.isGoing){
            System.err.println("Автобус в движении. Нельзя принимать пассажиров");
            return;
        }
        // проверяем, не превысили ли мы количество мест?
        if (this.count < this.passengers.length) {
            this.passengers[count] = passenger;
            this.count++;
        } else {
            System.err.println("Автобус переполнен!");
        }
    }

    public void setDriver(Driver driver) {
        if (this.isGoing){
            System.err.println("Автобус в движении. Нельзя менять водителя");
            return;
        }
        this.driver = driver;
        driver.setBus(this);
    }

    public boolean isFull() {
        return this.count == passengers.length;
    }

    public boolean isGoing() {
        return isGoing;
    }

    public void setGoing(boolean going) {
        isGoing = going;
        if (going) {
//            Когда автобус едет, все его пассажири называют свои имена.
            for (int i = 0; i < count; i++) {
                Passenger passenger = this.passengers[count];
                passenger.sayName();
            }
        }
    }
}
