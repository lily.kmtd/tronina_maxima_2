package Homework09;

/**
 * 30.10.2021
 * 12. Bus
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Driver {
    private String name;
    private int experience;
    private Bus bus;

    public Driver(String name, int experience) {
        this.name = name;
        if (experience > 0) {
            this.experience = experience;
        } else {
            System.err.println("ОПЫТА у " + name +  "НЕТ!");
        }
    }

    public void setBus(Bus bus) {
        this.bus = bus;
    }

    //У водителя сделать метод `drive`, который заставит "поехать" автобус
    public void drive() {
        if (this.bus != null) {
            bus.setGoing(true);
        } else {
            System.err.println("Для водителя не назначен автобус");
        }
    }

}
