package Homework11;

public class Program {
    public static void main (String []args) {
        testReverse();
    }

    private static void testArrayList() {
        ArrayList list = new ArrayList();
        list.add(3);
        list.add(1);
        list.add(5);
        list.add(7);
        list.add(3);
        list.add(9);
        list.add(1);
        list.add(7);
        list.print();

        list.remove(1);
        list.print();

        list.removeLast(1);
        list.print();

        list.removeAll(3);
        list.print();

        list.add(2, -1);
        list.print();
    }

    private static void testLinkedList() {
        LinkedList list = new LinkedList();
        list.add(1);
        list.add(4);
        list.add(2);
        list.add(1);
        list.add(4);
        list.add(1);

        list.remove(2);
        list.print();
        list.removeLast(4);
        list.print();
        list.removeAll(1);
        list.print();
    }

    private static void testReverse() {
        LinkedList list = new LinkedList();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(4);
        list.add(5);
        list.reverse();
        list.print();
    }
}
