package Homework11;
/**
 * 12.11.2021
 * 15. Lists
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class LinkedList {

    private static class Node {
        int value;
        Node next;

        Node(int value) {
            this.value = value;
        }
    }
    // ссылка на первый элемент
    private Node first;
    // ссылка на последний элемент
    private Node last;

    private int size = 0;

    public void add(int element) {
        // для нового элемента создаем узел
        Node newNode = new Node(element);
        // если список пустой
        if (isEmpty()) {
            // первый узел = новый узел
            first = newNode;
        } else {
            // следующий после последнего - это новый узел
            last.next = newNode;
        }
        // теперь новый узел - последний
        last = newNode;

        size++;
    }

    public void addToBegin(int element) {
        // для нового элемента создаем узел
        Node newNode = new Node(element);

        if (isEmpty()) {
            last = newNode;
        } else {
            // для нового узла, следующий после него - это первый узел списка
            newNode.next = first;
        }

        // теперь новый узел - первый
        first = newNode;
        size++;
    }

    public int get(int index) {
        // начинаем с первого элемента
        Node current = first;

        int i = 0;

        while (i < size) {
            // если нашли нужный индекс
            if (i == index) {
                // возвращаем значение текущего узла
                return current.value;
            }
            // иначе идем дальше
            current = current.next;
            i++;
        }

        System.err.println("В списке нет такого индекса");
        return -1;
    }

    public void removeAt(int index) {
        Node previousRemove = first;

        if (index == 0) {
            // если удаляется первый элемент, просто сдвигаем ссылку на него
            first = first.next;
            size--;
            return;
        }

        // доходим до индекса элемента, который стоит перед удаляемым
        for (int i = 0; i < index - 1; i++) {
            // переходим по узлам
            previousRemove = previousRemove.next;
        }
        // если удаляем последний
        if (index == (size - 1)) {
            last = previousRemove;
        }
        // в current находится узел, который стоит перед удаляемым
        Node forRemove = previousRemove.next;
        previousRemove.next = forRemove.next;

        size--;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    private boolean isCorrectIndex(int index) {
        return index >= 0 && index < size;
    }

    public void print() {
        Node current = first;
        while (current != null) {
            System.out.print(current.value + " ");
            current = current.next;
        }
        System.out.println("");
    }

    public void remove(int element) {
        if (first.value == element) {
            removeAt(0);
            return;
        }

        Node prev = first;
        Node current = first.next;
        while (prev.next != null) {
            if (current.value == element) {
                if (current == last) {
                    last = prev;
                    prev.next = null;
                } else {
                    prev.next = current.next;
                }
                size--;
                return;//удалили первый и закончили работу
            }
            prev = prev.next;
            current = current.next;
        }
    }

    public void removeLast(int element) {
        Node prevLastEntry = null;
        Node lastEntry = null;
        Node prev = first;
        Node current = first.next;
        while (prev.next != null) {
            if (current.value == element) {
                prevLastEntry = prev;
                lastEntry = current;
            }
            prev = prev.next;
            current = current.next;
        }
        prevLastEntry.next = lastEntry.next;
        if (lastEntry == last) {
            last = prevLastEntry;
        }
    }

    public void removeAll(int element) {
        if (first.value == element) {
            removeAt(0);
        }
        Node prev = first;
        Node current = first.next;
        while (prev.next != null) {
            if (current.value == element) {
                if (current == last) {
                    last = prev;
                    prev.next = null;
                } else {
                    prev.next = current.next;
                }
                size--;
            }
            prev = prev.next;
            current = current.next;
        }
    }

    public void add(int indexToAdd, int element) {
        if (!isCorrectIndex(indexToAdd)) {
            System.err.println("В списке нет такого индекса");
            return;
        }
        if (indexToAdd == 0) {
            addToBegin(element);
            return;
        }

        Node newNode = new Node(element);
        Node current = first;
        int idx = 0;
        while (idx < indexToAdd - 1) {
            current = current.next;
            idx++;
        }
        Node nextNode = current.next;

        current.next = newNode;
        newNode.next = nextNode;
        size++;
    }

    public void reverse() {
        if (size == 1) {
            return;
        }

        last = first;

        Node previous = null;
        Node current = first;

        while (current != null) {
            first = current.next;//новое начало неразвернуто списка
            current.next = previous;//развернули ссылку на предыдущий
            previous = current;
            current = first;
        }
        first = previous;//current.next == null
    }
}
