package Homework11;

/**
 * 09.11.2021
 * 15. Lists
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// список на основе массива
public class ArrayList {
    private final static int DEFAULT_SIZE = 10;

    // хранилище элементов
    private int[] elements;
    // количество фактических элементов в списке
    private int size;

    public ArrayList() {
        // при создании списка я создал внутри массив на 10 элементов
        this.elements = new int[DEFAULT_SIZE];
    }

    /**
     * Добавление элемента в конец списка
     * @param element добавляемый элемент
     */
    public void add(int element) {
        // если у меня переполнен массив
        if (isFullArray()) {
            resize();
        }

        this.elements[size++] = element;
    }

    private void resize() {
        // создаем новый массив, который в полтора раза больше, чем предыдущий
        int[] newArray = new int[this.elements.length + elements.length / 2];
        // копируем элементы из старого массива в новый поэлементно
        for (int i = 0; i < size; i++) {
            newArray[i] = this.elements[i];
        }
        // заменяем ссылку на старый массив ссылкой на новый массив
        // старый массив будет удален java-машиной
        this.elements = newArray;
    }

    private boolean isFullArray() {
        return size == elements.length;
    }

    /**
     * Получение элемента по индексу
     * @param index индекс элемента
     * @return элемент, который был добавлен в список под номером index, если такого индекса нет - ошибка
     */
    public int get(int index) {
        if (isCorrectIndex(index)) {
            return elements[index];
        } else {
            System.err.println("В списке нет такого индекса");
            return -1;
        }
    }

    private boolean isCorrectIndex(int index) {
        return index >= 0 && index < size;
    }

    /**
     * Удаляет все элементы из списка
     */
    public void clear() {
        size = 0;
    }

    /**
     * Возвращает количество элементов списка
     * @return размер списка
     */
    public int size() {
        return size;
    }

    /**
     * Удаляет элемент в заданном индексе, смещая элементы, которые идут после удаляемого на одну позицию влево
     *
     * 0 -> [14] 1-> [71] 2-> [82] 3-> [25], size = 4
     *
     * removeAt(1)
     *
     *
     * [14] [82] [25] | [25], size = 3
     *
     * @param index индекс удаляемого элемента
     */
    public void removeAt(int index) {
        if (!isCorrectIndex(index)) {
            System.err.println("В списке нет такого индекса");
            return;
        }

        for (int nextElIdx = index + 1; nextElIdx < size; nextElIdx++) {
            elements[nextElIdx - 1] = elements[nextElIdx];
        }
        size --;         
    }

    /**
     * Удаляет первое вхождение элемента в список
     *
     * 34, 56, 78, 56, 92, 11
     *
     * remove(56)
     *
     * 34, 78, 56, 92, 11
     *
     * @param element
     */
    public void remove(int element) {
        // find first idx
        for (int idx = 0; idx < size; idx++) {
            if (elements[idx] == element) {
                removeAt(idx);
                return;
            }
        }
    }

    /**
     * Удаляет последнее вхождение элемента в список
     *
     * 34, 56, 78, 56, 92, 11
     *
     * removeLast(56)
     *
     * 34, 56, 78, 92, 11
     *
     * @param element
     */
    public void removeLast(int element) {
        // find last idx
        for (int idx = size - 1; idx >= 0; idx--) {
            if (elements[idx] == element) {
                removeAt(idx);
                return;
            }
        }
    }

    public void removeAll(int element) {
        for (int idx = 0; idx < size; idx++) {
            if (elements[idx] == element) {
                removeAt(idx);
            }
        }
    }

    /**
     * Вставляет элемент в заданный индекс (проверяет условие, index < size)
     * Элемент, который стоял под индексом index сдвигается вправо (как и все остальные элементы)
     * 34, 56, 78, 56, 92, 11
     *
     * add(2, 100)
     *
     * 34, 56, 100, 78, 56, 92, 11
     *
     * @param index куда вставляем элемент
     * @param element элемент, который будем вставлять
     */
    public void add(int index, int element) {
        if (!isCorrectIndex(index)) {
            System.err.println("В списке нет такого индекса");
            return;
        }
        if (isFullArray()) {
            resize();
        }
        size++;
        int temp2 = element;
        for (int idx = index; idx < size; idx++) {
            int temp = get(idx);
            elements[idx] = temp2;
            temp2 = temp;
        }
    }

    public void print() {
        for (int i =0 ; i < size(); i++) {
            System.out.print(get(i) + " ");
        }
        System.out.println();
    }
}
