package Homework18;

import java.util.HashMap;
import java.util.Map;

public class Program {
    public static void main(String[] args) {
//        Считать из файла текст (ТОЛЬКО НА АНГЛИЙСКОМ), выяснить, какие слова встречаются чаще других и вывести их на экран.
        TextSource source = new TextSourceFileImpl("in.txt");
        String text = source.getText();

        Map<String, Integer> wordsOccurrences = getStatictics(text);

        System.out.println(wordsOccurrences);

    }

    private static Map<String, Integer> getStatictics(String text) {
        Map<String, Integer> wordsOccurrences = new HashMap<>();
        String[] wordsArray = text.split("\\W+");
        for(String word : wordsArray) {
            if (wordsOccurrences.containsKey(word)) {
                wordsOccurrences.put(word, wordsOccurrences.get(word) + 1);
            } else {
                wordsOccurrences.put(word, 1);
            }
        }
        return wordsOccurrences;
    }
}
