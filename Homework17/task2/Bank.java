package Homework17.task2;

import java.util.*;

/**
 * 21.12.2021
 * 21. Collection Task
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Bank {

    private Map<User, List<Transaction>> transactions;

    public Bank() {
        this.transactions = new HashMap<>();
    }

    private Set<User> getUsers() {
        return transactions.keySet();
    }

    private Optional<List<Transaction>> getUserTransactions(User user) {
        return Optional.ofNullable(transactions.get(user));
    }

    private void saveUserTransaction(User user, Transaction transaction) {
        Optional<List<Transaction>> transactionsOptional = getUserTransactions(user);
        List<Transaction> userTransactions = transactionsOptional.isPresent() ? transactionsOptional.get() : new ArrayList<>();
        if (userTransactions.contains(transaction)) {//duplicate
            return;
        }
        userTransactions.add(transaction);
        transactions.put(user, userTransactions);
    }

    // метод для перевода денег
    public void sendMoney(User from , User to, int sum) {
        // создаем сам перевод
        Transaction transaction = new Transaction(from, to, sum);
        saveUserTransaction(from, transaction);
    }

    // вернуть сумму всех транзакций по конкретному пользователю
    public int getTransactionsSum(User user) {
        List<Transaction> userTransactions =  transactions.get(user);
        int accumulator = 0;
        for (Transaction transaction : userTransactions) {
            accumulator += transaction.getSum();
        }
        return accumulator;
    }

    public int getTransactionsCount(User user) {
        List<Transaction> userTransactions =  transactions.get(user);
        return userTransactions.size();
    }

    //    Вывести все имена пользователей и количество их транзакций
    public void printUserWithTransactionsCount() {
        Set<User> users = getUsers();
        for (User user : users) {
            System.out.println(user + " : " + getTransactionsCount(user));
        }
    }

}
