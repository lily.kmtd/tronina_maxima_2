package Homework17.task2;

/**
 * 21.12.2021
 * 21. Collection Task
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {

    public static void main(String[] args) {
        // реализовать систему платежей
        // один пользователь может переводить деньги другому пользователю

        User marsel = new User("+79372824941", "Марсель");
        User rafael = new User("+79372824942", "Рафаэель");

        Bank bank = new Bank();
        bank.sendMoney(rafael, marsel, 100);
        bank.sendMoney(rafael, marsel, 300);
        bank.sendMoney(rafael, marsel, 200);
        bank.sendMoney(marsel, rafael, 50);
        bank.sendMoney(marsel, rafael, 100);
        bank.sendMoney(marsel, rafael, 120);

        System.out.println(bank.getTransactionsSum(marsel));
        System.out.println(bank.getTransactionsCount(marsel));

        System.out.println(bank.getTransactionsSum(rafael));
        System.out.println(bank.getTransactionsCount(rafael));

        bank.printUserWithTransactionsCount();
    }
}
