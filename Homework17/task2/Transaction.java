package Homework17.task2;

import java.util.Objects;
import java.util.StringJoiner;
import java.util.UUID;

/**
 * 21.12.2021
 * 21. Collection Task
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Transaction {
    private UUID transactionId;
    private User from;
    private User to;
    private int sum;

    public Transaction(User from, User to, int sum) {
        this.transactionId = UUID.randomUUID();
        this.from = from;
        this.to = to;
        this.sum = sum;
    }

    public User getFrom() {
        return from;
    }

    public User getTo() {
        return to;
    }

    public int getSum() {
        return sum;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Transaction.class.getSimpleName() + "[", "]")
                .add("from=" + from)
                .add("to=" + to)
                .add("sum=" + sum)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transaction that = (Transaction) o;
        return transactionId.equals(that.transactionId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(transactionId);
    }
}
