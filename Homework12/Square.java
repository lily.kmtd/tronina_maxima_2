package Homework12;

/**
 * 16.11.2021
 * 16. Inheritance and Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// квадрат
public class Square extends Rectangle {
    public Square(Point center, int a) {
        super(center, a, a);
    }

    public Square(int a) {
        this(new Point(0.0,0.0), a);
    }
}
