package Homework12;

/**
 * 16.11.2021
 * 16. Inheritance and Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Main {
    public static void main(String[] args) {
        Figure[] figures = new Figure[5];
        figures[0] = new Figure(new Figure.Point(-1,-1));
        figures[1] = new Rectangle(10, 15);
        figures[2] = new Square(new Figure.Point(10, 10), 5);
        figures[3] = new Ellipse(new Figure.Point(100, 100), 5,4);
        figures[4] = new Circle(10.0);

        Figure.Point newCenter = new Figure.Point(22,22);
        for (int i = 0 ; i < figures.length; i++) {
            figures[i].printPerimeter();
            figures[i].move(newCenter.x, newCenter.y);
        }
    }
}
