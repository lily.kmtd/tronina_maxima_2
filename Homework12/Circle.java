package Homework12;

/**
 * 16.11.2021
 * 16. Inheritance and Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// круг
public class Circle extends  Ellipse {
    public Circle(Point center, double r) {
        super(center, r, r);
    }

    public Circle(double r) {
        super(r, r);
    }
}
