package Homework12;

/**
 * 16.11.2021
 * 16. Inheritance and Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
// овал
public class Ellipse extends  Figure  {
    double r1;//большая полуось
    double r2;//малая полуось

    /**
     *
     * @param center центр эллипса
     * @param r1     большая полуось
     * @param r2     малая полуось
     */
    public Ellipse(Point center, double r1, double r2) {
        super(center);
        this.r1 = r1;
        this.r2 = r2;
    }

    public Ellipse(double r1, double r2) {
        this(new Point(0.0, 0.0), r1, r2);
    }

    @Override
    public double getPerimeter() {
        return 4*(Math.PI * r1 * r2 + (r1 - r2))/(r1 + r2);
    }
}
