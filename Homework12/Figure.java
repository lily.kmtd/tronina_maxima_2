package Homework12;

/**
 * 16.11.2021
 * 16. Inheritance and Polymorphism
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Figure {
    protected static class Point {
        double x;
        double y;

        Point(double x, double y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public String toString() {
            return "(" + x + ", " + y + ")";
        }
    }

    private Point center;

    public void setCenter(Point center) {
        this.center = center;
    }

    public Figure(Point center) {
        this.center = center;
    }

    public Figure() {
        this(new Point(0.0,0.0));
    }

    public double getPerimeter() {
        return -1;
    }

    /**
     * Перемещение фигуры в новый центр
     * @param newX
     * @param newY
     */
    public void move(double newX, double newY) {
        Point c = this.center;
        setCenter(new Point(newX, newY));
        System.out.println(this.getClass().getSimpleName()+" move:"+c.toString()+"=>"+this.center.toString());
    }

    public void printPerimeter() {
        System.out.println(this.getClass().getSimpleName()+" perimeter = "+getPerimeter());
    }

}
