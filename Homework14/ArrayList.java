package Homework14;

import java.util.Objects;
import java.util.Optional;

public class ArrayList<E> implements List<E> {
    private final static int DEFAULT_SIZE = 10;

    // хранилище элементов
    private E[] elements;
    // количество фактических элементов в списке
    private int size;

    public ArrayList() {
        // при создании списка я создал внутри массив на 10 элементов
        this.elements = (E[])new Object[DEFAULT_SIZE];
    }

    /**
     * Добавление элемента в конец списка
     *
     * @param element добавляемый элемент
     */
    @Override
    public void add(E element) {
        // если у меня переполнен массив
        ensureSize();

        this.elements[size++] = element;
    }

    public E get(int index) {
        if (!isCorrectIndex(index)) {
            System.err.println("В списке нет такого индекса");
            return null;
        }
        return this.elements[index];
    }

    @Override
    public int size() {
        return size;
    }

    private void ensureSize() {
        if (isFullArray()) {
            resize();
        }
    }

    private void resize() {
        // создаем новый массив, который в полтора раза больше, чем предыдущий
        E[] newArray = (E[])new Object[this.elements.length + elements.length / 2];
        // копируем элементы из старого массива в новый поэлементно
        for (int i = 0; i < size; i++) {
            newArray[i] = this.elements[i];
        }
        // заменяем ссылку на старый массив ссылкой на новый массив
        // старый массив будет удален java-машиной
        this.elements = newArray;
    }

    private boolean isFullArray() {
        return size == elements.length;
    }

    private boolean isCorrectIndex(int index) {
        return index >= 0 && index < size;
    }

    @Override
    public void removeAt(int index) {
        if (!isCorrectIndex(index)) {
            System.err.println("В списке нет такого индекса");
            return;
        }
        for (int idxAfterDelete = index + 1; idxAfterDelete < size; idxAfterDelete++) {
            elements[idxAfterDelete - 1] = elements[idxAfterDelete];
        }
        size --;
    }

    @Override
    public void remove(E element) {
        for (int idx = 0; idx < size; idx++) {
            if (Objects.equals(elements[idx],element)) {
                removeAt(idx);
            }
        }
    }

    @Override
    public void add(int indexToAdd, E element) {
        if (!isCorrectIndex(indexToAdd)) {
            System.err.println("В списке нет такого индекса");
            return;
        }
        if (isFullArray()) {
            resize();
        }
        size++;
        E current = element;
        for (int idx = indexToAdd; idx < size; idx++) {
            E temp = elements[idx];
            elements[idx] = current;
            current = temp;
        }
        elements[size] = current;
    }
}
