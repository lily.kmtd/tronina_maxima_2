package Homework14;

/**
 * 30.11.2021
 * 18. Generics
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class Program {

    public static void printList(List<String> strings) {
        for (int i = 0; i < strings.size(); i++) {
            System.out.print(strings.get(i));
        }
        System.out.println(" ");
    }

    public static void printSumOfElements(List<Integer> integers) {
        int sum = 0;
        for (int i = 0; i < integers.size(); i++) {
            sum += integers.get(i);
        }
        System.out.println(sum);
    }

    public static void main(String[] args) {
        List<String> stringsByArrayList = new ArrayList<>();
        stringsByArrayList.add("Hello");
        stringsByArrayList.add("Word");
        stringsByArrayList.add("Bye");
        stringsByArrayList.add(1,"Test");
        stringsByArrayList.remove("Hello");

        List<String> stringsLinkedList = new LinkedList<>();
        stringsLinkedList.add("Привет");
        stringsLinkedList.add("Слово");
        stringsLinkedList.add("Пока");
        stringsLinkedList.add(1,"Test");
        stringsLinkedList.remove("Слово");

        List<Integer> integersByArrayList = new ArrayList<>();
        integersByArrayList.add(100);
        integersByArrayList.add(200);
        integersByArrayList.add(300);
        integersByArrayList.add(1,777);
        integersByArrayList.remove(200);

        List<Integer> integersByLinkedList = new LinkedList<>();
        integersByLinkedList.add(400);
        integersByLinkedList.add(500);
        integersByLinkedList.add(600);
        integersByLinkedList.add(2,777);
        integersByLinkedList.remove(400);

        printList(stringsLinkedList);
        printList(stringsByArrayList);
        printSumOfElements(integersByArrayList);
        printSumOfElements(integersByLinkedList);

    }
}
