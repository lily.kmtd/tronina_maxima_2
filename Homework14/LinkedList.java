package Homework14;

import java.util.Objects;

/**
 * 12.11.2021
 * 15. Lists
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class LinkedList<E> implements List<E> {

    // параметризованный узел
    private static class Node<V> {
        V value;
        Node<V> next;

        Node(V value) {
            this.value = value;
        }
    }

    // ссылка на первый элемент
    private Node<E> first;
    // ссылка на последний элемент
    private Node<E> last;

    private int size = 0;

    @Override
    public void add(E element) {
        // для нового элемента создаем узел
        Node<E> newNode = new Node<>(element);
        // если список пустой
        if (isEmpty()) {
            // первый узел = новый узел
            first = newNode;
        } else {
            // следующий после последнего - это новый узел
            last.next = newNode;
        }
        // теперь новый узел - последний
        last = newNode;

        size++;
    }

    public E get(int index) {
        // начинаем с первого элемента
        Node<E> current = first;

        int i = 0;

        while (i < size) {
            // если нашли нужный индекс
            if (i == index) {
                // возвращаем значение текущего узла
                return current.value;
            }
            // иначе идем дальше
            current = current.next;
            i++;
        }

        return null;
    }

    @Override
    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public void removeAt(int index) {
        Node<E> previousRemove = first;

        if (index == 0) {
            // если удаляется первый элемент, просто сдвигаем ссылку на него
            first = first.next;
            size--;
            return;
        }

        // доходим до индекса элемента, который стоит перед удаляемым
        for (int i = 0; i < index - 1; i++) {
            // переходим по узлам
            previousRemove = previousRemove.next;
        }
        // если удаляем последний
        if (index == (size - 1)) {
            last = previousRemove;
        }
        // в current находится узел, который стоит перед удаляемым
        Node<E> forRemove = previousRemove.next;
        previousRemove.next = forRemove.next;

        size--;

    }

    @Override
    public void remove(E element) {
        if (first.value == element) {
            removeAt(0);
            return;
        }

        Node<E> prev = first;
        Node<E> current = first.next;
        while (prev.next != null) {
            if (Objects.equals(current.value, element)) {
                if (current == last) {
                    last = prev;
                    prev.next = null;
                } else {
                    prev.next = current.next;
                }
                size--;
            }
            prev = prev.next;
            current = current.next;
        }
    }

    @Override
    public void add(int indexToAdd, E element) {
        // для нового элемента создаем узел
        Node<E> newNode = new Node<>(element);
        // если список пустой
        if (isEmpty()) {
            // первый узел = новый узел
            first = newNode;
        } else if (indexToAdd == size - 1) {//последний
            last.next = newNode;
            last = newNode;
        } else {
            Node<E> currentNode = first;
            for (int i = 0; i < indexToAdd - 1; i++) {
                // переходим по узлам
                currentNode = currentNode.next;
            }
            newNode.next = currentNode.next;
            currentNode.next = newNode;

        }
        size++;
    }
}
