package Homework14;
/**
 * 04.12.2021
 * 18. Generics
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface List<T> {
    void add(T element);

    T get(int index);

    int size();

    void removeAt(int index);

    void remove(T element);

    void add(int index, T element);
}
