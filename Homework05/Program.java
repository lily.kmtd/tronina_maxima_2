package com.trainee.Homework05;

import java.util.Scanner;

public class Program {
    //Задание 05 - ArrayExtended
    //На вход подается список возрастов людей (мин 0 лет, макс 120 лет)
    //Необходимо вывести информацию о том, какой возраст чаще всего встречается.

    public static void main(String []args){
        int endOfLine = -1;
        int arraySize = 121;
        int[] array = new int[arraySize];

        Scanner scanner = new Scanner(System.in);
        int enteredNum = scanner.nextInt();
        while (enteredNum != endOfLine) {
            array[enteredNum] += 1;
            enteredNum = scanner.nextInt();
        }

        //Вывод
        int maxOccuranceAge = 0;
        int maxOccuranceValue  = 0;
        for (int age = 0; age < arraySize; age++) {
            if (array[age] > maxOccuranceValue) {
                maxOccuranceAge = age;
                maxOccuranceValue = array[age];
            }
        }
        System.out.println("Чаще остальных встречается возраст: "+ maxOccuranceAge);
    }
}
