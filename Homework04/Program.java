package Homework04;

import java.util.Scanner;

public class Program {
    //Задание 04 - Array
    //Объявить массив (размер - 10). Заполнить его элементами из консоли.
    //Вывести в обратном порядке.
    public static void main(String []args){
        int arraySize = 10;
        int[] array = new int[arraySize];

        Scanner scanner = new Scanner(System.in);
        int currentElementIdx = 0;
        while (currentElementIdx < arraySize) {
            array[currentElementIdx] = scanner.nextInt();
            currentElementIdx++;
        }

        //Вывод
        int lastElementIdx = arraySize - 1;
        while (lastElementIdx >= 0) {
            System.out.println(array[lastElementIdx]);
            lastElementIdx--;
        }
    }
}
