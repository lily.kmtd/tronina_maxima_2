package Homework15_16;

/**
 * 18.12.2021
 * 20. Map
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public interface Set<E> {
    void add(E element);

    boolean contains(E element);
}
