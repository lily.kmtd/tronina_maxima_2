package Homework15_16;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 * 25.12.2021
 * 20. Map
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
public class main {
//привет привет марсель в казани ты тоже в казани
    public static void main(String[] args) {

        List<String> words = readWords();

        Map<String, Integer> counts = new MapHashImpl<>();
        for (String word : words) {
            int lastCount = 0;
            if (counts.containsKey(word)) {
                lastCount = counts.get(word);//ранее сохраненное значение
            }
            lastCount++;
            counts.put(word, lastCount);
        }
    }

    private static List<String> readWords() {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        input = input.replaceAll("[^\\da-zA-Zа-яёА-ЯЁ ]", "");
        List<String> words = Arrays.asList(input.split(" "));
        return words;
    }
}
